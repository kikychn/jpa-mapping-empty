package com.twuc.webApp.entity;

import com.twuc.webApp.dao.StaffRepository;
import net.minidev.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class StaffTest {
    @Autowired
    StaffRepository repo;

    @Autowired
    EntityManager em;

    @Test
    void should_save_an_Staff_entity() {
        Staff staff = new Staff(1L, "zheng", "piaopiao");
        assertNotNull(staff);
    }

    @Test
    void should_persist_an_staff_entity_with_em() {
        em.persist(new Staff(1L, "zheng", "piaopiao"));
        Staff staff = em.find(Staff.class, 1L);

        assertEquals("zheng", staff.getName().getFirstName());
        assertEquals("piaopiao", staff.getName().getLastName());
    }

    @Test
    void should_persist_an_staff_entity_with_repo() {
        repo.save(new Staff(1L, "zheng", "piaopiao"));
        Staff staff = repo.findById(1L).orElseThrow(RuntimeException::new);
            assertEquals("zheng", staff.getName().getFirstName());
            assertEquals("piaopiao", staff.getName().getLastName());
    }

}