package com.twuc.webApp.entity;

import com.twuc.webApp.dao.OfficeRepository;
import com.twuc.webApp.exception.CityIsNUll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ALL")
@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
class OfficeTest {
    @Autowired
    private OfficeRepository repo;

    @Autowired
    private EntityManager em;

    @Test
    void hello_world() {
        assertTrue(true);
    }

    @Test
    void should_save_an_office_entity() {
        Office savedOffice = repo.save(new Office(1L, "chengdu"));
        em.flush();
        assertNotNull(savedOffice);
    }

    @Test
    void should_persist_an_office_entity() {
        em.persist(new Office(1L, "Xi'an"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class, 1L);

        assertEquals("Xi'an", office.getCity());
        assertEquals(Long.valueOf(1), office.getId());
    }

    @Test
    void should_throw_exception_when_office_city_is_null() {
        assertThrows(DataIntegrityViolationException.class, ()->{
            repo.save(new Office(1L, null));
            repo.flush();
        });
    }

    @Test
    void should_throw_exception_when_city_is_longer_than_36() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            repo.save(new Office(1L, "111111111111111111111111111111111111111111111"));
            repo.flush();
        });

    }
}