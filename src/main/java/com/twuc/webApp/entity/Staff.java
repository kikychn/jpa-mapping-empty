package com.twuc.webApp.entity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Staff {
    @Id
    private Long id;
    @Embedded
    private Name name;

    public Staff() {
    }

    public Staff(Long id, String firstName, String lastName) {
        this.id = id;
        this.name = new Name(firstName,lastName);
    }

    public Long getId() {
        return id;
    }

    public Name getName() {
        return name;
    }
}
